// eslint-disable-next-line import/no-extraneous-dependencies
import { describe, it, expect } from '@jest/globals';

import * as R from 'ramda';
import * as D from 'date-fns/fp';

import { createRunner } from '../src';
import { defaultEnv } from '../src/env';

describe('lambda', () => {
  const ploson = createRunner({ staticEnv: { ...defaultEnv, D, R } });

  it('should evaluate lambda function with arguments (with defaults)', async () => {
    expect.assertions(1);

    const result = await ploson([
      ',',
      {
        reducer: [
          '=>',
          // ['acc', 'x'],
          ['acc', ['x', 1]],
          ['R.append', ['R.when', ['R.gt', 'R.__', 4], ['R.divide', 'R.__', 2], '$x'], '$acc'],
        ],
      },
      { process: ['R.reduce', '$reducer', ['of']] },
      // eslint-disable-next-line no-null/no-null
      ['$process', ['of', 1, 3, 4, 6, 9, null, undefined, NaN, 0, '`foo`']],
    ]);

    expect(result).toStrictEqual([1, 3, 4, 3, 4.5, 1, 1, 1, 0, 'foo']);
  });

  it('should evaluate lambda function with object syntax inside', async () => {
    expect.assertions(1);

    const result = await ploson([
      ',',
      ['setVar', '`bar`', 3],
      [['=>', [], [',', { bar: ['R.multiply', 2, '$bar'] }]]],
      '$bar',
    ]);

    expect(result).toBe(6);
  });

  it('should evaluate lambda function default arguments', async () => {
    expect.assertions(1);

    const result = await ploson([
      ',',
      ['setVar', '`bar`', 3],
      [['=>', [['x', ['R.add', '$bar', 2]]], ['R.multiply', 2, '$x']]],
    ]);

    expect(result).toBe(10);
  });

  it('should handle different/short arguments/format', async () => {
    expect.assertions(4);
    await expect(ploson([['=>']])).resolves.toBeUndefined();
    await expect(ploson([['=>', 'Math.PI']])).resolves.toBe(Math.PI);
    await expect(ploson([['=>', 'x', 'Math.PI']])).resolves.toBe(Math.PI);
    await expect(ploson([['=>', () => 5, 'Math.PI']])).resolves.toBeInstanceOf(Error);
  });
});
