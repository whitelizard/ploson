// eslint-disable-next-line import/no-extraneous-dependencies
import { describe, it, expect } from '@jest/globals';

import * as R from 'ramda';
import * as D from 'date-fns/fp';

import { createRunner } from '../src';
import { envPlugin, defaultEnv } from '../src/env';
import { evaluatePlugin } from '../src/evaluate';
import { varsPlugin } from '../src/vars';

/*
- Test RegExp
- Test all functions in defaultEnv
- Test bad plugin (e.g. returning event ERROR with string)
- Controlled alternatives to setTimeout/setInterval? As plugin? To defaultEnv?
- Variable scope/Namespace hierarchy?
*/

const wait = R.curry((ms, val) => new Promise((r) => setTimeout(() => r(val), ms)));
// const delay = wait(R.__, undefined);

/* Xeslint-disable jest/no-commented-out-tests */
describe('types', () => {
  const ploson = createRunner();

  it('should handle number', async () => {
    expect.assertions(2);
    await expect(ploson(2)).resolves.toBe(2);
    await expect(ploson(3.14)).resolves.toBe(3.14);
  });

  it('should handle bool', async () => {
    expect.assertions(1);
    await expect(ploson(true)).resolves.toBe(true);
  });

  it('should handle nil', async () => {
    expect.assertions(2);
    // eslint-disable-next-line no-null/no-null
    await expect(ploson(null)).resolves.toBeNull();
    await expect(ploson(undefined)).resolves.toBeUndefined();
  });

  it('should handle string', async () => {
    expect.assertions(1);
    await expect(ploson('`hello world`')).resolves.toBe('hello world');
  });

  it('should handle Object', async () => {
    expect.assertions(1);
    await expect(ploson({ a: 1 })).resolves.toStrictEqual({ a: 1 });
  });

  it('should be able to create Array with Array.of in env', async () => {
    expect.assertions(3);
    await expect(ploson(['of', 1, 2, 3])).resolves.toStrictEqual([1, 2, 3]);
    await expect(ploson(['of', 1, true, '`foo`'])).resolves.toStrictEqual([1, true, 'foo']);
    await expect(ploson(['of'])).resolves.toStrictEqual([]);
  });
});

describe('default syntax & features', () => {
  const ploson = createRunner({ staticEnv: { foo: 5, '+': (a, b) => a + b, R } });

  it('should replace identifiers found in env', async () => {
    expect.assertions(2);
    await expect(ploson('foo')).resolves.toBe(5);
    await expect(ploson(['+', 'foo', 2])).resolves.toBe(7);
  });

  it('should handle nested calls', async () => {
    expect.assertions(1);
    await expect(
      ploson(['R.filter', ['R.gt', 'R.__', 7], ['R.map', ['R.add', 3], ['of', 3, 4, 5]]]),
    ).resolves.toStrictEqual([8]);
  });

  it('should not evaluate empty array', async () => {
    expect.assertions(2);
    await expect(ploson([])).resolves.toStrictEqual([]);
    await expect(ploson(['of'])).resolves.toStrictEqual([]);
  });
});

describe('vars', () => {
  const ploson = createRunner({
    staticEnv: { ...defaultEnv, foo: 5, '+': (a, b) => a + b, $bar: 8 },
    plugins: [envPlugin(), evaluatePlugin(), varsPlugin({ vars: { val: 4 } })],
  });

  it('should set and get vars', async () => {
    expect.assertions(1);
    await expect(
      ploson([',', { add: '+', foo: ['+', '$val', 'foo'] }, { result: ['$add', '$foo', 2] }]),
    ).resolves.toHaveProperty('result', 11);
  });

  it('should support deep set', async () => {
    expect.assertions(1);
    await expect(ploson([',', ['setVar', '`a.b.c`', 5], '$a.b.c'])).resolves.toBe(5);
  });

  it('should preserve vars', async () => {
    expect.assertions(2);
    await expect(ploson([',', { test: { a: { b: ['+', 3, 2] } } }, '$test.a.b'])).resolves.toBe(5);
    await expect(ploson('$test')).resolves.toStrictEqual({ a: { b: 5 } });
  });

  it('should have explicit getter & setter', async () => {
    expect.assertions(2);
    await expect(ploson(['setVar', '`test`', 1])).resolves.toBe(1);
    await expect(ploson(['getVar', '`test`', 2])).resolves.toBe(1);
  });

  it('should block forbidden identifiers in getter', async () => {
    expect.assertions(1);
    await expect(
      ploson([',', { foo: '+' }, ['getVar', '`foo.prototype.constructor`']]),
    ).resolves.toBeInstanceOf(Error);
  });

  it('should block forbidden identifiers in setter', async () => {
    expect.assertions(1);
    // eslint-disable-next-line
    expect(await ploson(['setVar', '`eval`', eval])).toBeInstanceOf(Error);
  });

  it('should handle empty var name', async () => {
    expect.assertions(2);
    await expect(ploson([',', { '': 2 }, '$'])).resolves.toBe(2);
    await expect(ploson(['getVar', '``'])).resolves.toBe(2);
  });

  it('should evaluate vars before env', async () => {
    expect.assertions(2);
    await expect(ploson(['getVar', '`bar`'])).resolves.toBeUndefined();
    await expect(ploson('$bar')).resolves.toBeUndefined();
  });
});

describe('plugins isolated', () => {
  describe('env', () => {
    const ploson = createRunner({ staticEnv: { foo: 2 }, plugins: [envPlugin()] });

    it('should error for unfound identifier', async () => {
      expect.assertions(2);

      const result = await ploson('test');

      expect(result).toBeInstanceOf(Error);
      expect(result).toHaveProperty('message', "Unfound identifier 'test'");
    });

    it('should replace known identifiers', async () => {
      expect.assertions(2);
      await expect(ploson('foo')).resolves.toBe(2);
      await expect(ploson('void')).resolves.toBeInstanceOf(Function);
    });
  });

  describe('evaluate', () => {
    const ploson = createRunner({ plugins: [evaluatePlugin()] });

    it('should evaluate strings', async () => {
      expect.assertions(2);
      await expect(ploson('`foo`')).resolves.toBe('foo');
      await expect(ploson('```')).resolves.toBe('`');
    });

    it('should evaluate functions', async () => {
      expect.assertions(1);
      await expect(ploson([() => 5])).resolves.toBe(5);
    });
  });

  describe('vars', () => {
    const vars = { foo: 5 };
    const ploson = createRunner({ plugins: [varsPlugin({ prefix: '@@', vars })] });

    it('should evaluate vars with prefix', async () => {
      expect.assertions(1);
      await expect(ploson('@@foo')).resolves.toBe(5);
    });

    it('should not be able to access getVar/setVar without env plugin', async () => {
      expect.assertions(1);

      const result = await ploson(['setVar', '`foo`', 3]);

      expect(result).toBeInstanceOf(Error);
    });

    it('should be able to set vars through objects', async () => {
      expect.assertions(4);
      expect(vars).toHaveProperty('foo', 5);

      const result = await ploson({ foo: 6 });

      expect(result).toStrictEqual({ foo: 6 });
      expect(vars).toHaveProperty('foo', 6);
      await expect(ploson('@@foo')).resolves.toBe(6);
    });
  });
});

describe('extensions', () => {
  const ploson = createRunner({ staticEnv: { ...defaultEnv, D, R } });

  it('should use env util libs', async () => {
    expect.assertions(1);
    await expect(
      ploson([',', { result: ['D.addSeconds', ['R.sum', ['of', 1, 2]], ['newDate']] }, '$result']),
    ).resolves.toBeInstanceOf(Date);
  });
});

describe('security', () => {
  // eslint-disable-next-line
  const ploson = createRunner({ staticEnv: { ...defaultEnv, D, R, eval } });

  it('should block eval even if in staticEnv', async () => {
    expect.assertions(1);
    await expect(ploson(['eval', '`1`'])).resolves.toBeInstanceOf(Error);
  });
});

describe('error handling', () => {
  const ploson = createRunner({ staticEnv: { ...defaultEnv, D, R } });

  it('should error for unfound identifier', async () => {
    expect.assertions(2);

    const result = await ploson('foo');

    expect(result).toBeInstanceOf(Error);
    expect(result).toHaveProperty('message', "Unfound identifier 'foo'");
  });

  it('should error for trying to execute non-function', async () => {
    expect.assertions(1);
    await expect(ploson([5])).resolves.toBeInstanceOf(Error);
  });

  it('should return error from executed function', async () => {
    expect.assertions(2);

    const result = await ploson(['R.pick', '`x`', 5]);

    expect(result).toBeInstanceOf(Error);
    expect(result).toHaveProperty('message', "Cannot use 'in' operator to search for 'x' in 5");
  });

  it('should propagate deep error', async () => {
    expect.assertions(2);

    const result = await ploson(['void', ['R.pick', '`x`', 5]]);

    expect(result).toBeInstanceOf(Error);
    expect(result).toHaveProperty('message', "Cannot use 'in' operator to search for 'x' in 5");
  });
});

describe('async', () => {
  const ploson = createRunner({ staticEnv: { wait, R } });

  it('should handle basic async', async () => {
    expect.assertions(1);
    await expect(
      ploson([
        ',',
        {
          delayedValue100: ['R.pipe', ['wait', 100], ['R.andThen', ['setVar', '`foo`']]],
          delayedValue50: ['R.pipe', ['wait', 50], ['R.andThen', ['setVar', '`foo`']]],
        },
        { _1: ['$delayedValue100', '`late`'], _2: ['$delayedValue50', '`early`'] },
        '$foo',
      ]),
    ).resolves.toBe('late');
  });
});
