// eslint-disable-next-line import/no-extraneous-dependencies
import { describe, it, expect } from '@jest/globals';

import * as R from 'ramda';
import * as D from 'date-fns/fp';

import { createRunner } from '../src';
import { envPlugin, defaultEnv } from '../src/env';
import { evaluatePlugin } from '../src/evaluate';
import { varsPlugin } from '../src/vars';

const wait = R.curry((ms, val) => new Promise((r) => setTimeout(() => r(val), ms)));

describe('examples from README', () => {
  it('should run README Object section examples', async () => {
    expect.assertions(8);

    const ploson = createRunner({
      staticEnv: {
        ...defaultEnv,
        D,
        R,
        fetchProfile: R.pipe(
          wait(100),
          R.andThen(R.prop(R.__, { 'john@doe.ex': { name: 'John Doe' } })),
        ),
        fetchConfiguration: R.pipe(
          wait(100),
          R.andThen(() => ({ defaultName: 'Noname' })),
        ),
      },
      plugins: [envPlugin(), evaluatePlugin(), varsPlugin({ vars: { uId: 'john@doe.ex' } })],
    });

    await expect(ploson({ a: 1 })).toStrictEqual({ a: 1 });
    await expect(ploson({ a: '`hello world`' })).toStrictEqual({ a: 'hello world' });
    await expect(ploson({ a: ['of', 1, 2, 3] })).toStrictEqual({ a: [1, 2, 3] });
    await expect(ploson({ a: '`foo`', b: '`bar`' })).toStrictEqual({ a: 'foo', b: 'bar' });
    await expect(ploson({ a: ['of', 1, 2, '$b'] })).toStrictEqual({ a: [1, 2, 'bar'] });
    await expect(
      ploson([
        ',',
        { user: ['fetchProfile', '$uId'], conf: ['fetchConfiguration'] }, // parallell async
        { name: ['R.propOr', '$conf.defaultName', '`name`', '$user'] },
      ]),
    ).toStrictEqual({ name: 'John Doe' });
    await expect(
      ploson({ a: '$a', b: '$b', user: '$user', conf: '$conf', name: '$name' }),
    ).toStrictEqual({
      a: [1, 2, 'bar'],
      b: 'bar',
      user: { name: 'John Doe' /* ... */ },
      conf: { defaultName: 'Noname' /* ... */ },
      name: 'John Doe',
    });
    await expect(
      ploson([
        ',',
        { user: ['fetchProfile', '`foo`'], conf: ['fetchConfiguration'] }, // parallell async
        { name: ['R.propOr', '$conf.defaultName', '`name`', '$user'] },
      ]),
    ).toStrictEqual({ name: 'Noname' });
  });

  it('should run README non-covered examples', async () => {
    expect.assertions(3);

    const ploson = createRunner({
      staticEnv: { ...defaultEnv, D, R },
      plugins: [envPlugin(), evaluatePlugin(), varsPlugin({ vars: { uId: 'john@doe.ex' } })],
    });

    await expect(ploson(['R.map', ['R.add', 3], ['of', 3, 4, 5]])).toStrictEqual([6, 7, 8]);
    await expect(
      ploson([
        ',',
        {
          data: ['of', 3, 4, 5],
          func: ['R.map', ['R.add', 3]],
        },
        ['$func', '$data'],
      ]),
    ).toStrictEqual([6, 7, 8]);
    await expect(ploson(['R.add', 7, ['Math.floor', ['R.divide', 37, 2]]])).toBe(
      Math.floor(37 / 2) + 7,
    );
  });
});
