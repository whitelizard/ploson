import * as R from 'ramda';
import { bi } from 'tri-fp';
import isString from 'lodash.isstring';
import isFunction from 'lodash.isfunction';

// import { stringify } from './utils';

// const unProcessedTypes = ['Boolean', 'Number', 'Null', 'Undefined', 'Function', 'RegExp'];
// const isUnProcessedType = R.pipe(R.type, R.includes(R.__, unProcessedTypes));

const shouldEvaluateString = R.both(R.startsWith('`'), R.endsWith('`'));
const extractStr = R.slice(1, -1);

export const evaluatePlugin = () => ({
  onLeave: ({ state, current }) => {
    if (state !== 'untouched') return undefined;
    if (isString(current)) {
      const isStrConst = shouldEvaluateString(current);
      return isStrConst
        ? {
            type: 'REPLACE',
            current: extractStr(current),
          }
        : undefined;
      // return { type: 'REPLACE', current: envHas(current) ? getFromEnv(current) : undefined };
    }
    if (Array.isArray(current) && current.length > 0) {
      const [fn, ...args] = current;
      // console.log('EVALUATE FUNC:', originalNode?.[0], fn, isFunction(fn), args);
      if (!isFunction(fn))
        return {
          type: 'ERROR',
          error: Error(`'${fn}' is not a function`),
        };
      const [error, result] = bi(fn)(...args);
      // console.log('EVALUATE FUNC RESULT:', result);
      // console.log('EVALUATE FUNC ERROR:', error);
      if (error) return { type: 'ERROR', error };
      return { type: 'REPLACE', current: result };
    }
    return undefined;
  },
});
