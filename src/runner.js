import * as R from 'ramda';
import { hasPath, mapAsync } from 'rambdax';
import isPlainObject from 'lodash.isplainobject';
import isFunction from 'lodash.isfunction';
import isString from 'lodash.isstring';
import isError from 'lodash.iserror';
// import deepFreeze from 'deep-freeze-strict';

import {
  OBJ,
  getPath,
  ensureError,
  parallelAwaitObjectVals,
  addStack,
  maybeWrap,
  maybeUnwrap,
} from './utils';
import { envPlugin } from './env';
import { evaluatePlugin } from './evaluate';
import { varsPlugin } from './vars';
import { lambdaPlugin } from './lambda';

const pluginEnvs = R.pipe(R.pluck('staticEnv'), R.mergeAll);

const UNTOUCHED = 'untouched';
const REPLACED = 'replaced';
const PROTECTED = 'protected';
const ERROR = 'error';

const pluginCallbackReducer =
  (ctx) =>
  ({ state, current, error }, fn) => {
    if (state === ERROR) return { state, error };
    const iterObj = fn({ ...ctx, state, current });
    if (iterObj?.type === 'ERROR') return { state: ERROR, error: iterObj?.error };
    // if (state === PROTECTED) return { state, error };
    if (iterObj?.type === 'REPLACE') {
      // console.log('REPLACING WITH:', iterObj?.current);
      return { state: REPLACED, current: iterObj?.current };
    }
    if (iterObj?.type === 'PROTECT') return { state: PROTECTED, current: iterObj?.current };
    return { state, current };
  };

export const createRunner = ({
  staticEnv = OBJ,
  plugins = [lambdaPlugin(), envPlugin(), evaluatePlugin(), varsPlugin()],
} = OBJ) => {
  // -------------------------------------------
  // PARSER WIDE IDENTIFIERS

  const env = { ...pluginEnvs(plugins), ...staticEnv }; // TODO: deepFreeze?
  const envHas = (pth) => hasPath(pth, env);
  const getFromEnv = getPath(env);
  const onEnterFns = R.pipe(R.pluck('onEnter'), R.filter(isFunction))(plugins);
  const onLeaveFns = R.pipe(R.pluck('onLeave'), R.filter(isFunction))(plugins);

  const processOnEnterFns = (reduceArgs) =>
    R.reduce(pluginCallbackReducer({ envHas, getFromEnv, ...reduceArgs }), {
      state: UNTOUCHED,
      current: reduceArgs.originalNode,
    })(onEnterFns);

  const processOnLeaveFns = (reduceArgs, state) =>
    R.reduce(pluginCallbackReducer({ envHas, getFromEnv, ...reduceArgs }), {
      state,
      current: reduceArgs.node,
    })(onLeaveFns);

  // -------------------------------------------
  // NODE PROCESSOR (SYNCRONOUS)

  const processNode = (originalNode) => {
    // TODO: freeze originalNode
    // ENTER
    const onEnterCtx = processOnEnterFns({ processNode, originalNode });
    if (onEnterCtx.state === ERROR) return addStack(originalNode)(ensureError(onEnterCtx.error));

    // eslint-disable-next-line functional/no-let
    let node = onEnterCtx.current;
    // RECURSE if not PROTECTED
    if (onEnterCtx.state !== PROTECTED) {
      if (Array.isArray(originalNode) || isPlainObject(originalNode)) {
        node = R.map(processNode, node);
        const firstError = R.find(isError, Object.values(node));
        if (firstError) return addStack(originalNode)(firstError);
      }
    }

    // LEAVE
    const onLeaveCtx = processOnLeaveFns({ processNode, originalNode, node }, onEnterCtx.state);
    if (onLeaveCtx.state === ERROR) return addStack(originalNode)(ensureError(onLeaveCtx.error));

    const unfoundIdentifier = isString(onLeaveCtx.current) && onLeaveCtx.state === UNTOUCHED;
    if (unfoundIdentifier)
      return addStack(originalNode)(Error(`Unfound identifier '${onLeaveCtx.current}'`));

    return onLeaveCtx.current;
  };

  // -------------------------------------------
  // ASYNC NODE PROCESSOR

  const processNodeAsync = async (originalNode) => {
    // TODO: freeze originalNode
    // ENTER
    const onEnterCtx = processOnEnterFns({ processNode, processNodeAsync, originalNode });
    if (onEnterCtx.state === ERROR) return addStack(originalNode)(ensureError(onEnterCtx.error));

    // eslint-disable-next-line functional/no-let
    let node = onEnterCtx.current;
    // RECURSE if not PROTECTED
    if (onEnterCtx.state !== PROTECTED) {
      if (Array.isArray(originalNode) || isPlainObject(originalNode)) {
        node = R.map(maybeUnwrap)(await mapAsync(processNodeAsync, node));
        const firstError = R.find(isError, Object.values(node));
        if (firstError) return addStack(originalNode)(firstError);
      }
      // PARALLEL AWAIT POSSIBLE PROMISES
      if (isPlainObject(node)) node = await parallelAwaitObjectVals(node);
    }

    // LEAVE
    const onLeaveCtx = processOnLeaveFns(
      { processNode, processNodeAsync, originalNode, node },
      onEnterCtx.state,
    );
    if (onLeaveCtx.state === ERROR) return addStack(originalNode)(ensureError(onLeaveCtx.error));

    const unfoundIdentifier = isString(onLeaveCtx.current) && onLeaveCtx.state === UNTOUCHED;
    if (unfoundIdentifier)
      return addStack(originalNode)(Error(`Unfound identifier '${onLeaveCtx.current}'`));

    return maybeWrap(onLeaveCtx.current);
  };

  // -------------------------------------------
  // createRunner RETURN

  return R.pipe(processNodeAsync, maybeUnwrap);
};
