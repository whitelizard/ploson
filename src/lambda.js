import * as R from 'ramda';
import isString from 'lodash.isstring';

import { ensureArray } from './utils';

const addShortSupport = (node) => (node.length === 2 ? [node[0], [], node[1]] : node);

export const lambdaPlugin = () => {
  return {
    onEnter: ({ originalNode, current, state, processNode }) => {
      if (state !== 'untouched') return undefined;
      if (Array.isArray(current) && current[0] === '=>') {
        // if (originalNode.length < 3)
        //   return { type: 'ERROR', error: Error('Missing argument for lambda syntax') };
        const [, argTemplate = [], cmd] = addShortSupport(originalNode);
        if (!(isString(argTemplate) || Array.isArray(argTemplate)))
          return {
            type: 'ERROR',
            error: Error('Second argument for lambda syntax must be string or Array'),
          };
        return {
          type: 'PROTECT',
          current: (...args) => {
            const setArgs = (setter) =>
              R.addIndex(R.map)((arg, idx) => {
                const [name, defau] = ensureArray(arg);
                return setter(name, R.defaultTo(processNode(defau), args[idx]));
              }, ensureArray(argTemplate));
            return processNode([',', [setArgs, 'setVar'], cmd]);
          },
        };
      }
      return undefined;
    },
  };
};
