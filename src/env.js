import * as R from 'ramda';
import crossFetch from 'cross-fetch';
import isString from 'lodash.isstring';

// import { OBJ } from './utils';

export const lastArg = R.pipe(Array.of, R.last);

// export const operators = {
//   '>': R.gt,
//   '<': R.lt,
//   '<=': R.lte,
//   '>=': R.gte,
//   '==': R.equals,
//   '!=': R.complement(R.equals),
//   '!': R.not,
//   // '===': R.curry((a, b) => a === b),
//   // '!==': R.curry((a, b) => a !== b),
// };

export const defaultEnv = {
  undefined,
  console,
  Array,
  Object,
  String,
  Number,
  Boolean,
  Promise,
  newPromise: (...args) => new Promise(...args),
  Date,
  newDate: (...args) => new Date(...args),
  Math,
  parseInt,
  parseFloat,
  Set,
  Map,
  newSet: (...args) => new Set(...args),
  newMap: (...args) => new Map(...args),
  RegExp,
  performance,
  // fetch,
};

export const fetch = crossFetch;

export const envPlugin = () => ({
  staticEnv: {
    // lastArg,
    ',': lastArg,
    of: Array.of,
    void: () => undefined,
    // noop: () => undefined,
  },
  onLeave: ({ state, current, envHas, getFromEnv }) => {
    if (state !== 'untouched') return undefined;
    if (isString(current) && envHas(current)) {
      // return envHas(current) ? getFromEnv(current) : undefined;
      // console.log('ENV:', current, getFromEnv(current));
      return { type: 'REPLACE', current: getFromEnv(current) };
    }
    return undefined;
  },
});
