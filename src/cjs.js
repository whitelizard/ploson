import * as ploson from './index.js';

if (typeof module < 'u') module.exports = ploson;
else self.ploson = ploson;
