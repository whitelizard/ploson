import * as R from 'ramda';
import { path } from 'rambdax';
// import get from 'lodash.get';
// import { bi } from 'tri-fp';
import isString from 'lodash.isstring';
import isError from 'lodash.iserror';
import isPlainObject from 'lodash.isplainobject';
import isFunction from 'lodash.isfunction';
import isBoolean from 'lodash.isboolean';
import isNumber from 'lodash.isnumber';
import isNull from 'lodash.isnull';
import isUndefined from 'lodash.isundefined';

// eslint-disable-next-line no-null/no-null
export const OBJ = Object.freeze(Object.create(null));
export const ARR = Object.freeze([]);
export const FUNC = Function.prototype;

export const ensureArray = R.unless(Array.isArray, R.of);

export const forbiddenIdentifiers = [
  'eval',
  'Function',
  'constructor',
  'setTimeout',
  'setInterval',
];
export const isForbidden = R.pipe(
  R.when(isString, R.split('.')),
  R.anyPass(R.map(R.includes)(forbiddenIdentifiers)),
);
export const getPath = (obj) => (pth) =>
  isForbidden(pth) ? Error(`${pth} is forbidden`) : path(pth, obj);
// isForbidden(pth) ? Error(`${pth} is forbidden`) : get(obj, pth);

export const ensureError = (error) => {
  return isError(error)
    ? error
    : isString(error)
      ? Error(error)
      : Error('Unknown error from plugin');
};

export const parallelAwaitObjectVals = async (obj) => {
  const orderedPairs = R.toPairs(obj);
  const keys = R.pluck(0)(orderedPairs);
  const vals = R.pluck(1)(orderedPairs);
  const resolvedValues = await Promise.all(vals);
  return R.zipObj(keys, resolvedValues);
};

// export const safeJsonStringify = bi(JSON.stringify);

// export const directJsonStringify = (node) => {
//   const [jsonError, result] = safeJsonStringify(node);
//   return !jsonError ? result : node?.toString ? node.toString() : node;
// };

export const stringify = R.cond([
  [isString, (x) => `"${x}"`],
  [isNumber, R.identity],
  [isNull, () => 'null'],
  [isUndefined, () => 'undefined'],
  [isBoolean, R.identity],
  [isFunction, () => '<FUNC>'],
  [Array.isArray, (x) => `[${x.map(stringify).join(', ')}]`],
  [
    isPlainObject,
    R.pipe(
      R.toPairs,
      R.reduce((arr, [k, v]) => [...arr, `${k}: ${stringify(v)}`], []),
      (x) => `{ ${x.join(', ')} }`,
    ),
  ],
]);

// {
//   const type = R.type(node);
//   const strategies = {
//     Undefined: R.identity,
//     Null: R.identity,
//     Number: R.identity,
//     Boolean: R.identity,
//     String: R.identity,
//     Array: directJsonStringify,
//     Object: directJsonStringify,
//     Function: R.always('[func]'),
//     RegExp: directJsonStringify,
//   };
//   return (strategies[type] ?? R.identity)(node);
// }

export const addStack = (originalNode) => (error) => {
  /* eslint-disable no-param-reassign */
  error.plosonStack = [...(error.plosonStack ?? []), `${stringify(originalNode)}`];
  return error;
  /* eslint-enable no-param-reassign */
};

const promiseWrapperKey = '@@promise/wrapped';
// const maybeWrap = R.when(R.is(Promise), R.objOf(promiseWrapperKey)); // Prevented use of R.__
export const maybeWrap = (node) =>
  typeof node?.then === 'function' ? { [promiseWrapperKey]: node } : node;
export const maybeUnwrap = (node) => {
  if (!isPlainObject(node)) return node;
  const keys = Object.keys(node);
  return keys.length === 1 && keys[0] === promiseWrapperKey ? node[promiseWrapperKey] : node;
};
