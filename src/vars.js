import * as R from 'ramda';
import set from 'lodash.set';
import isPlainObject from 'lodash.isplainobject';
import isString from 'lodash.isstring';
import isError from 'lodash.iserror';

import { OBJ, getPath } from './utils';

const checkAndOmitPrefix =
  (prefix = '') =>
  (path) => {
    const isVar = R.startsWith(prefix, path);
    return [isVar ? R.drop(prefix.length, path) : path, isVar];
  };

export const varsPlugin = ({ prefix = '$', vars = {} } = OBJ) => {
  const getVarPath = checkAndOmitPrefix(prefix);
  const getVar = R.when(R.complement(isError), getPath(vars));
  // const hasVar = TODO: implement and use in onLeave return
  const setVar = R.curry((pth, val) => {
    const res = getPath(set(vars, pth, val))(pth);
    // console.log('setVar:', res, pth, val, vars);
    return res;
  });
  const setVarFlipped = (val, key) => setVar(key, val);
  return {
    staticEnv: { getVar, setVar },
    onEnter: ({ current, state }) => {
      if (state !== 'untouched') return undefined;
      if (isString(current)) {
        const [path, isVar] = getVarPath(current);
        // console.log('VAR:', current, vars, path, getVar(path));
        return isVar ? { type: 'REPLACE', current: getVar(path) } : undefined;
        // return { type: 'REPLACE', current: isVar ? getVar(path) : undefined };
      }
      return undefined;
    },
    onLeave: ({ current }) => {
      // console.log('varsPlugin onLeave:', node, resolvedNode, altered);
      // TODO: If found in vars but altered -- still return vars version?
      if (isPlainObject(current)) R.forEachObjIndexed(setVarFlipped, current);
      // console.log(vars);
      return undefined;
    },
  };
};
